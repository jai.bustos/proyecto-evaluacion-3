from django.contrib import admin
from .models import TipoProducto,Producto, Contacto
# Register your models here.
class ProductoAdmin(admin.ModelAdmin):
    list_display= ['nombre','marca','tipo_producto']
    list_filter=['tipo_producto']
    list_per_page=5

admin.site.register(TipoProducto)
admin.site.register(Producto,ProductoAdmin)
admin.site.register(Contacto)