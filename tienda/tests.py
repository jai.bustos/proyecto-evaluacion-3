from django.test import TestCase

# Create your tests here.
from django.urls import reverse, resolve

class TestUrls:

    def test_detail_url(self):
        path = reverse('listado_producto', kwargs={'pk': 1})
        assert resolve(path).view_name == 'listado_producto'

