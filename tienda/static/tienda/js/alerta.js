function confirmarEliminar(id) {
    Swal.fire({
        title: '¿Estas seguro  ?',
        text: "¡no podras revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, eliminar!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
            window.location.href ="/eliminar-producto/"+id+"/";
        }
      })
}