var staticCacheName = "tiendaonline-v" + new Date().getTime();

// La ruta de los archivos que quiero cachear
var filesToCache = [
    '/offline',
    '/static/css/estilotienda.css',
    '/static/css/estilos.css',
    '/static/img/logo.png',
    '/static/img/icono.jpg',
];

// Cache al instalar (se mantiene el código original)
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Limpiar cache al activar (se mantiene código original)
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("tiendaonline-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Servicio desde el cache (código actualizado para que cachee todos los elementos con clone)
self.addEventListener("fetch", event => {
    event.respondWith(
        fetch(event.request).then((result)=>{
            return caches.open(staticCacheName).then(function(c) {
                c.put(event.request.url, result.clone())
                return result;
            })
        }).catch(function(e){
            return caches.match(event.request)
        })
    )
});
