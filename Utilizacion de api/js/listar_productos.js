$(document).ready(function(){
    obtenerlistado();
});

function obtenerlistado(){
    $.get({
        url:'https://jabustos.pythonanywhere.com/api/productos/',
        success: function(respuesta){
            var listadoproductos=respuesta
        armadoListado(listadoproductos)

        },
        error:function(error){
            console.error("servicio de listar con error")
            console.error(error)
        }

    });
}

function armadoListado(productos){
    console.log(productos)
    var contenedor = $('#contenedor_productos')
    contenedor.empty()
    $.each(productos,function(i, producto){
        var vista = vistaProducto(producto)
        contenedor.append(vista)
    })
}

function vistaProducto(producto){

    var div = $('<div></div>').addClass('producto')
    var img = $('<img>');
    img.attr('src', producto.imagen);
    img.attr('alt', producto.nombre);
    div.append(img)

    var div2 = $ ('<div></div>').addClass('detalle')
    var titulo = $ ('<h5></h5>')
    titulo.text(producto.marca)
    div2.append(titulo)
    var titulo2 = $ ('<h2></h2>')
    titulo2.text(producto.nombre)
    div2.append(titulo2)
    div.append(div2);

    return div
}